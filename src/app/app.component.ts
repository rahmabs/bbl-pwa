import { Component } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { CheckForUpdateService } from './shared/services/check-for-update.service';
import { MatSnackBar } from '@angular/material';
import { get, set } from 'idb-keyval';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'my-pwa-app';
  updateAvailable = false;

  // a toaster that will notify the user that there’s an update, and let them reload themselves whenever they’re ready
  constructor(private updates: SwUpdate, private dialog: MatSnackBar, private checkForUpdateService: CheckForUpdateService) {
    this.updates.available.subscribe((event) => {
      console.log('avaiable!');
      this.dialog.open('Newer version of the app is available', 'Refresh').onAction().subscribe(() => {
        window.location.reload();
      });

      this.updateAvailable = true;
    });
    this.showIosInstallBanner();
  }

  async showIosInstallBanner() {
    // Detects if device is on iOS
    const isIos = () => {
      const userAgent = window.navigator.userAgent.toLowerCase();
      return /iphone|ipad|ipod/.test(userAgent);
    };
    // Detects if device is in standalone mode
    const isInStandaloneMode = () => ('standalone' in (window as any).navigator) && ((window as any).navigator.standalone);

    // Show the banner once
    const iosBannerIsShown = await get('iosBannerIsShown');

    // Checks if should display install popup notification:
    if (isIos() && !isInStandaloneMode() && iosBannerIsShown === undefined) {
      this.dialog.open('install', 'tap the icon below to add to home screen ').onAction().subscribe(() => {
      });
      set('iosBannerIsShown', true);
    }
  }
}
