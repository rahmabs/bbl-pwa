export interface Item{
    name: string;
    username: string;
    email: string;
    website: string;
    phone: string;
  }