import { ApplicationRef, Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { concat, interval } from 'rxjs';
import { first } from 'rxjs/operators';

@Injectable()
export class CheckForUpdateService {
// service that will check for an update every 6 hours. Let’s use this service!
// The checkForUpdateService is loaded, and is checking every 6 hours for an update
    constructor(appRef: ApplicationRef, updates: SwUpdate) {
        console.log('loaded');
        // Allow the app to stabilize first, before starting polling for updates with `interval()`.
        const appIsStable$ = appRef.isStable.pipe(first(isStable => isStable === true));
        const everySixHours$ = interval(6 * 60 * 60 * 1000);
        const everySixHoursOnceAppIsStable$ = concat(appIsStable$, everySixHours$);

        console.log('test');

        everySixHoursOnceAppIsStable$.subscribe(() => updates.checkForUpdate());
    }
}
