import { Injectable } from '@angular/core';

import { Item } from '../models/item.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private dataURL = 'https://jsonplaceholder.typicode.com/users';
  constructor(private httpClient: HttpClient) {

  }

  fetch(): Observable<Item[]> {
    return  this.httpClient.get(this.dataURL) as Observable<Item[]>;
  }
}
